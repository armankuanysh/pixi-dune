Here is a little plugin for PIXI.js v4.8.7 to create animation with particle system.

To use this plugin you need to install PIXI.js v4.8.7:

`npm install pixi.js@4.8.7`

Then in your main javascript file you need to import the Dune.js file(Dune.js and Particle.js files should be in the same directory!):

`import Dune from './Dune';`

Then you need to create the markup in your HTML file:

```
'<div class="container">
    <img id="myImage" src="./your_image.png" alt="this is your image" />
</div>'
```


After that you need to select your image and pass it as argument to Dune, with sizes of the image: 

```
const img = document.querySelector('#myImage');

const dune = new Dune(img, width, height);
```


Thats all!